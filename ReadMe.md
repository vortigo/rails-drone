# Vortigo - Teste prático - Ruby on Rails

Parabéns! Você chegou até o nosso teste para desenvolvedores Ruby on Rails.

Realizamos este teste para avaliarmos o seu nível de conhecimento em tecnologias e desafios que você virá a encontrar caso faça parte da nossa equipe.

Para isso, preparamos um teste prático de rápido desenvolvimento, mas que é mais do que suficiente para nos introduzir a quem você é escrevendo código: sua organização de projeto, formatação, arquitetura e conceitos de desenvolvimento. 🤘

## Contexto:

Implementar uma página web para o controle de posição de um drone em um plano cartesiano (X, Y).

Essa página deve conter um campo de texto para receber a string de entrada, um botão de enviar coordenadas e, conforme achar melhor, deve responder o plano cartesiano calculado.

O ponto inicial do drone é `(0, 0)` para cada execução.

A string de entrada pode conter os seguintes caracteres **N**, **S**, **L**, e **O** representando **Norte**, **Sul**, **Leste** e **Oeste** respectivamente.
Estes catacteres podem estar presentes aleatóriamente na string de entrada.
Uma string de entrada `NNNLLL` irá resultar em uma posição final `(3, 3)`, assim como uma string `NLNLNL` irá resultar em `(3, 3)`.

Caso o caracter **X** esteja presente, o mesmo irá cancelar a operação anterior. 
Caso houver mais de um caracter X consecutivo, o mesmo cancelará mais de uma ação na quantidade em que o X estiver presente.
Uma string de entrada `NNNXLLLXX` irá resultar em uma posição final `(1, 2)` pois a string poderia ser simplificada para `NNL`.

Além disso, um número pode estar presente após o caracter da operação, representando o "passo" que a operação deve acumular.
Este número deve estar compreendido entre `1` e `2147483647`.
Deve-se observar que a operação `X` não suporta opção de *passo* e deve ser considerado inválido. Uma string de entrada `NNX2` deve ser considerada inválida.
Uma string de entrada `N123LSX` irá resultar em uma posição final `(1, 123)` pois a string pode ser simplificada para `N123L`
Uma string de entrada `NLS3X` irá resultar em uma posição final `(1, 1)` pois a string pode ser siplificada para `NL`.

Caso a string de entrada seja inválida ou tenha algum outro problema, o resultado deve ser `(999, 999)`.

## Observações:

Realizar uma implementação com padrões de código para ambiente de "produção". 

Comentar o código explicando o que for relevânte para a solução do problema.

Adicionar testes unitários para alcançar uma cobertura de testes relevânte.

## Submissão

- Criar um repositório privado com o nome `dev-rails-vortigo` no Gitlab, compartilhando-o com os usuários @douglas.amengual e @jeferson.raupp (com permissão Developer).

## Contato

Buscamos o máximo de clareza possível ao documentar o exercício e esperamos que todas as dúvidas possam ser solucionadas com este `README` mas, as dúvidas virão e estamos prontos! Fique à vontade para entrar em contato conosco através do endereço [jeferson.raupp@vortigo.com.br](mailto:jeferson.raupp@vortigo.com.br) para tirar dúvidas ou pedir ajuda. Pedimos apenas que as dúvidas sejam pertinentes ao exercício, para que possamos dar toda a atenção necessária a todos os candidatos.

## Dica:

Os teste unitários esperados são:
```
Quando a string de entrada for: NNNNNLLLLL
O retorno esperado é: (5, 5)

Quando a string de entrada for: NLNLNLNLNL
O retorno esperado é: (5, 5)

Quando a string de entrada for: NNNNNXLLLLLX
O retorno esperado é: (4, 4)

Quando a string de entrada for: SSSSSOOOOO
O retorno esperado é: (-5, -5)

Quando a string de entrada for: S5O5
O retorno esperado é: (-5, -5)

Quando a string de entrada for: NNX2
O retorno esperado é: (999, 999)

Quando a string de entrada for: N123LSX
O retorno esperado é: (1, 123)

Quando a string de entrada for: NLS3X
O retorno esperado é: (1, 1)

Quando a string de entrada for: NNNXLLLXX
O retorno esperado é: (1, 2)

Quando a string de entrada for: N40L30S20O10NLSOXX
O retorno esperado é: (21, 21)

Quando a string de entrada for: NLSOXXN40L30S20O10
O retorno esperado é: (21, 21)

Quando a string de entrada for: nula
O retorno esperado é: (999, 999)

Quando a string de entrada for: vazia
O retorno esperado é: (999, 999)

Quando a string de entrada for: espaços em branco
O retorno esperado é: (999, 999)

Quando a string de entrada for: 123N
O retorno esperado é: (999, 999)

Quando a string de entrada for: N2147483647N
O retorno esperado é: (999, 999)

Quando a string de entrada for: NNI
O retorno esperado é: (999, 999)

Quando a string de entrada for: N2147483647XN
O retorno esperado é: (0, 1)
```